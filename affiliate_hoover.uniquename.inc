<?php

/**
* @file
* Allows the uploading of new CSV files#
*/



/**
* Array for finding if string is found in array produced by get_varialbe
*
* Empty e-mail addresses are allowed. See RFC 2822 for details.
*
* @param $referencia
*   A string taken from the URL
* $array
* An array from the get_variable row
*
* @return
*   TRUE if found, and FALSE if not
*/

function affiliate_hoover_in_array_like($referencia, $array) {

    foreach ($array as $ref) {
        if (stristr($referencia, $ref)) {
            return true;
        }
    }

    return false;

}


/**
* checks that URL item is actual form. returns false if not and is used in a 404 message below
*
* @param $args
*   From menu 'page arguments'. Section of URL
* 
* @return
*   TRUE if found and returns form, FALSE if not present and creates a 404 page
*/

function affiliate_hoover_unique_name($args) {

    $aff = variable_get('affiliate_hoover');

    $result = affiliate_hoover_in_array_like($args, $aff);

    if (!$result) {

        return drupal_not_found();
        drupal_exit();

    } else {

        return drupal_get_form('affiliate_hoover_uniquename_build_form');

    }
    // end if

}
// end if affiliate_hoover_unique_name()


/**
* Function that is called by drupal_get_form in affiliate_hoover_unique_name
* 
* @return
*   form array
*/

function affiliate_hoover_uniquename_build_form($form, &$form_state) {

    $form['affiliate_hoover_unique_name_form'] = array(
        '#type' => 'item',
        '#title' => t('Feed details for '.arg(4)),
        '#description' => 'Upload a CSV file here',
        '#attributes' => array('enctype' => 'multipart/form-data'),
        );

    $form['affiliate_hoover_unique_name_form']['first_part'] = array(
        '#type' => 'fieldset',
        '#title' => '',
        '#description' => '',
        );

    $form['affiliate_hoover_unique_name_form']['first_part']['ah_file'] = array(
        '#type' => 'file',
        '#title' => t('Upload file'),
        '#description' => t('<p>Upload your CSV file here.</p><p>The maximum file upload is '.(file_upload_max_size
            () / 1048576).'MBs</p>'),

        );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
        );

    return $form;

}


/**
* Implementation of hook_validate
*
* @param $args
*   From menu 'page arguments'. Section of URL
* 
*/

function affiliate_hoover_uniquename_build_form_validate($form, &$form_state) {

    $validators = array(
        'file_validate_extensions' => array('csv'),
        'file_validate_size' => array(file_upload_max_size()),
        'affiliate_hoover_uniquename_build_form_file_validate' => array(arg(4)),
        );

    // Additional validation functions in core:
    // file_validate_size();
    // file_validate_is_image();
    // file_validate_extensions();
    // file_validate_image_resolution();
    // file_validate_name_length();

    $file = file_save_upload('ah_file', $validators, FALSE, FILE_EXISTS_REPLACE);

    if (isset($file)) {
        // File upload was attempted.
        if ($file) {
            // Put the temporary file in form_values so we can save it on submit.
            $form_state['values']['ah_file'] = $file;
        } else {
            form_set_error('ah_file', t('The file could not be uploaded.'));
        }
    }
}


/**
* Implementation of hook_submit()
*
*/
function affiliate_hoover_uniquename_build_form_submit($form, &$form_state) {

    // Check if a file was uploaded.
    if ($file = $form_state['values']['ah_file']) {

        // Create the directory if it doesn't exist
        $directory = 'public://affiliate_hoover';
        file_prepare_directory($directory, FILE_CREATE_DIRECTORY);

        // Copy the file over.
        $filename = file_unmanaged_copy($file->uri, $directory, FILE_EXISTS_REPLACE);

        $header_array = affiliate_hoover_parse_csv_head($file->uri);

        $header_array_amend = $header_array;
        //count total number of entries
        $num_rows = affiliate_hoover_count_csv_rows($file->uri);
        foreach ($header_array_amend as $key => $value) {
            $header_array_amend['[#'.$key.'#]'] = $value;
            unset($header_array_amend[$key]);
        }
        // end foreach

        // check databse table to see if the entry already exists.
        // If not create new entry
        // If it does then update

        $result = db_query('SELECT n.name, n.fileName FROM {ah_feed_details} n WHERE n.name = :name',
            array(':name' => arg(4)))->fetchObject();

        if ($result != FALSE) {

            //$record = $result->fetchObject();

            if ($result->fileName != $file->filename) {

                // delete old file if the file name for the feed has changed

                file_unmanaged_delete('public://affiliate_hoover/'.$result->fileName);

            }

            db_update('ah_feed_details')->fields(array(
                'fileName' => $file->filename,
                'header_array' => serialize($header_array),
                'header_array_amend' => serialize($header_array_amend),
                'num_rows' => $num_rows,
                'url' => $filename,
                ))->condition('name', arg(4))->execute();

        } else {

            // create new entry

            db_insert('ah_feed_details')->fields(array(
                'name' => arg(4),
                'fileName' => $file->filename,
                'header_array' => serialize($header_array),
                'header_array_amend' => serialize($header_array_amend),
                'num_rows' => $num_rows,
                'url' => $filename,
                ))->execute();

        }

        drupal_set_message(t("Your file has been uploaded!"));

        $form_state['redirect'] = array( // $path
                'admin/config/affiliate_hoover/uniqueform/'.arg(4), );

    }

}


/**
* uses the File_CSV_DataSource to find all headers in a CSV file
*
* @param $file
*   path to file
* 
* @return
*   array of headers if TRUE, FALSE if file cannot be loaded
*/

function affiliate_hoover_parse_csv_head($file) {

    $csv = new File_CSV_DataSource;

    if ($csv->load($file)) {

        return $csv->getHeaders();

    } else {

        return FALSE;

    }
    // end if ($csv->sadfsad
}


/**
* uses the File_CSV_DataSource to count all rows in a CSV file
*
* @param $file
*   path to file
* 
* @return
*   number of rows if TRUE, FALSE if file cannot be loaded
*/


function affiliate_hoover_count_csv_rows($file) {

    $csv = new File_CSV_DataSource;

    if ($csv->load($file)) {

        return $csv->countRows();

    } else {

        return FALSE;

    }
    // end if ($csv->sadfsad
}


/**
 * Implements hook_file_validate().
 *
 */

function affiliate_hoover_uniquename_build_form_file_validate($file, $string) {

    $return = db_query('SELECT n.fileName FROM {ah_feed_details} n WHERE n.name <> :name', array(':name' =>
            $string))->fetchAll();

    $error = array();

    foreach ($return as $key => $value) {

        if ($file->filename == $value->fileName) {

            $error[] = t('This file is already uploaded: %string', array('%string' => $file->
                    filename));

        }

    }

    return $error;
}


/**
 * Menu title callback
 *
 */

function affiliate_hoover_uniquename_title_callback($args) {

    return t($args);

}
