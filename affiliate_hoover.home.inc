<?php

/**
 * @file
 * Form for central page of Affiliate Hoover
 * 
 * Updates variable_get('affiliate_hoover')
 */

function affiliate_hoover_first_form_render_array() {

    $build = array('string_paragraph' => array(
            '#type' => 'markup',
            '#markup' => '<p>Please read the '.AH_HELP.
                ' if you are unsure about how to use this module',
            ), );

    return drupal_render($build);
}


/**
 * Implements form_hook())
 * 
 * @return
 *   Form array
 */

function affiliate_hoover_first_form($form, &$form_state) {

    //variable_del('affiliate_hoover');
    $form['affiliate_hoover_first_form'] = array(
        '#type' => 'item',
        '#title' => t('Add feed names'),
        '#post_render' => array('affiliate_hoover_first_form_render_array'),
        );

    static $i = 1;

    $form_data = variable_get('affiliate_hoover');

    if ($form_data != NULL) {

        // reset array so first key has a value of 1
        $keys = range(1, count($form_data));
        $values = array_values($form_data);
        $form_data = array_combine($keys, $values);

        foreach ($form_data as $key => $value) {

            // loop through array to create the form

            $result = db_query('SELECT n.name FROM {ah_feed_details} n WHERE n.name = :name', array
                (':name' => $value))->fetchColumn();

            $html = ($result != '') ? l('Edit feed form for '.$value,
                'admin/config/affiliate_hoover/uniqueform/'.$value) : null;

            $html .= '<br />';

            $html .= l('Upload feed file for '.$value, 'admin/config/affiliate_hoover/uniquename/'.
                $value);

            $form['feed_name'.$key] = array(
                '#title' => t('Feed details for '.$value),
                '#type' => 'fieldset',
                );

            $form['feed_name'.$key][$key] = array(
                '#title' => t('Name of feed:'),
                '#type' => 'textfield',
                '#description' => t('Please enter a unique name for the feed.'),
                '#required' => true,
                '#default_value' => $value,
                '#prefix' => $html,
                '#disabled' => TRUE,
                );

            $form['feed_name'.$key]['checkbox'.$key] = array(
                '#title' => t('Delete entry'),
                '#type' => 'checkbox',
                '#description' => t('Delete above feed. This will delete all details about this feed from the database and the uploaded file, if any'),
                '#required' => false,
                '#default_value' => !empty($_REQUEST['feed_name']['checkbox'.$key]) ? $_REQUEST['feed_name']['checkbox'.
                    $key] : '',
                );

        }

    }

    $form['add_feed'] = array(
        '#title' => t('Add a new feed name below'),
        '#type' => 'fieldset',
        );

    $form['add_feed']['0'] = array(
        '#title' => t('Add feed here:'),
        '#type' => 'textfield',
        '#description' => t('Please enter a unique name for the feed.'),
        '#required' => false,
        '#default_value' => !empty($_REQUEST['feed_name']['0']) ? check_plain(strip_tags($_REQUEST['feed_name']['0'])) :
            '',
        );

    $form['add'] = array(
        '#type' => 'submit',
        '#value' => t('Add'),
        '#submit' => array('affiliate_hoover_first_form_add_submit'),
        );

    $form['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('affiliate_hoover_first_form_delete_submit'),
        );

    return $form;

} // end affiliate_hoover_first_form


/**
 * Used in affiliate_hoover_first_form_validate. Checks to see if checkbox has been clicked
 *
 * @param $form
 *   form array
 * 
 * @return
 *   string with digit of checkbox
 */

function affiliate_hoover_find_checkbox_digit($form) {

    // If checkbox is set then find digit of the connected input field

    $pattern = '/^checkbox/';

    foreach ($form as $key => $value) {

        if (preg_match($pattern, $key) && $value == '1') {

            // remove entry from array of checkbox is set to true
            if (preg_match($pattern, $key)) {

                if ($value == '1') {

                    return preg_replace('/[^0-9]|,[0-9]*$/', '', $key);

                }

            } // end if if (preg_match($pattern, $key)) {

        } // end if (is_int($key) || preg_match($pattern, $key)) {

    } // end foreach

}


/**
 * implements hook_validate())
 *
 */

function affiliate_hoover_first_form_validate($form, &$form_state) {

    // check with database to make sure that name is unique

    $tmp = array();

    $digit = affiliate_hoover_find_checkbox_digit($form_state['values']);

    foreach ($form_state['values'] as $key => $value) {

        if (is_int($key)) {

            // if digit is set then remove the associated input from the array

            if (isset($digit)) {
                if ((int)$key == (int)$digit) continue;
            }

            if ($value == '') continue;

            $tmp[] = $value;

            if (!ctype_alnum($value)) {

                form_set_error('feed_name', 'Please don\'t include any special characters or spaces');
                break;

            } // end if if (!ctype_alnum($value)) {

        } // end if(is_int($key) || preg_match($pattern, $key)) {

    } // end foreach

    // this is a nice little trick to make sure that all values in the array are unique

    if (count($tmp) != count(array_unique($tmp))) {

        form_set_error('feed_name', 'Please make sure all feed names are unique');

    }

} // end affiliate_hoover_first_form_validate

/**
 * implements hook_submit())
 * 
 * Only creates items 
 *
 */

function affiliate_hoover_first_form_add_submit($form, &$form_state) {

    $an_array = array();

    foreach ($form_state['values'] as $key => $value) {

        if (is_int($key)) {

            if ($value == '') continue;

            $sanitized_data = stripslashes(trim($value));

            $an_array[] = $sanitized_data;

            variable_set('affiliate_hoover', $an_array);

        } // end if(is_int($key) || preg_match($pattern, $key)) {

    } // end foreach

    if (isset($sanitized_data)) {

        drupal_set_message(t('Returned data here: @return', array('@return' => $sanitized_data)));

    }

} // end affiliate_hoover_first_form_submit

/**
 * implements hook_submit())
 * 
 * Will delete AND create items
 *
 */

function affiliate_hoover_first_form_delete_submit($form, &$form_state) {

    $an_array = array();

    $digit = affiliate_hoover_find_checkbox_digit($form_state['values']);

    foreach ($form_state['values'] as $key => $value) {

        if (is_int($key)) {

            // if digit is set then remove the associated input from the array

            if (isset($digit)) {
                if ((int)$key == (int)$digit) continue;
            }

            if ($value == '') continue;

            $sanitized_data = stripslashes(trim($value));

            $an_array[] = $sanitized_data;

            variable_set('affiliate_hoover', $an_array);

            affiliate_hoover_delete_all_data($an_array);

        } // end if(is_int($key) || preg_match($pattern, $key)) {

    } // end foreach

    if (isset($sanitized_data)) {

        drupal_set_message(t('Returned data here: @return', array('@return' => $sanitized_data)));

    }

} // end affiliate_hoover_first_form_submit

/**
 * deletes corresponding data for delete form items
 * this includes the entry from ah_feed_details and the actual file
 *  does not delete individual posts
 *
 * @param $an_array
 *
 * 
 * @return
 *   deletes entry in ah_feed_details table and its file
 */

function affiliate_hoover_delete_all_data($an_array) {

    $return = db_query('SELECT n.name FROM {ah_feed_details} n')->fetchAllAssoc('name');

    if ($odd = affiliate_hoover_arrayDiffEmulation(array_keys($return), $an_array)) {

        $new_return = db_query('SELECT n.fileName FROM {ah_feed_details} n WHERE n.name = :name',
            array(':name' => $odd))->fetchObject();

        if (isset($new_return->fileName)) {

            file_unmanaged_delete('public://affiliate_hoover/'.$new_return->fileName);

        }

        db_delete('ah_feed_details')->condition('name', $odd)->execute();

    }

}


/**
 * a faster implementation of the PHP function array_diff
 *
 */

function affiliate_hoover_arrayDiffEmulation($arrayFrom, $arrayAgainst) {

    $arrayAgainst = array_flip($arrayAgainst);

    foreach ($arrayFrom as $key => $value) {
        if (isset($arrayAgainst[$value])) {
            unset($arrayFrom[$key]);
        }
    }

    return implode('', array_values($arrayFrom));
}

/**
 * title callback from the menu
 *
 */

function affiliate_hoover_titles() {

    return t('Affilate Hoover home page');

}
