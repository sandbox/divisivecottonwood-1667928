<?php

/**
* @file
* The theme system, which controls the output of Drupal.
*
* The theme system allows for nearly all output of the Drupal system to be
* customized by user themes.
*/



/**
* Function used in affiliate_hoover_unique_form
*
* @param $referencia
*   A string taken from the URL
* $array
* An array from the get_variable row
*
* @return
*   TRUE if found, and FALSE if not
*/

function affiliate_hoover_in_array_like($referencia, $array = array()) {

    foreach ($array as $ref) {
        if (stristr($referencia, $ref)) {
            return true;
        }
    }

    return false;

}


/**
* checks that URL item is actual form. returns false if not and is used in a 404 message below
*
* @param $args
*   From menu 'page arguments'. Section of URL
* 
* @return
*   TRUE if found and returns form, FALSE if not present and creates a 404 page
*/

function affiliate_hoover_unique_form($args) {

    $aff = db_query('SELECT n.name FROM {ah_feed_details} n WHERE n.name = :name', array(':name' =>
            arg(4)))->fetchAssoc();

    if ($aff) {

        $result = affiliate_hoover_in_array_like($args, $aff);

    } else {

        $result = FALSE;

    }

    if (!$result || !$aff) {

        return drupal_not_found();
        drupal_exit();

    } else {

        return drupal_get_form('affiliate_hoover_uniqueform_build_form');

    } // end if

} // end if affiliate_hoover_unique_name()


/**
* database call used in the two functions proceeding it
*
*/

function affiliate_hoover_return_form_details() {
    
    return db_query('SELECT n.* FROM {ah_feed_details} n WHERE n.name = :name', array(':name' =>
            arg(4)))->fetchObject();
    
}


/**
* Uses Drupal render array to format HTML correctly
*
* 
* @return
*   render array
*/

function affiliate_hoover_pages_render_array() {

    $form_headers = affiliate_hoover_return_form_details();

    $list = array();

    foreach (unserialize($form_headers->header_array_amend) as $key => $value) {
        $list[] .= $key.' = '.$value;
    }

    $build = array(
        'string_paragraph' => array(
            '#type' => 'markup',
            '#markup' => '<p>Please read the '.AH_HELP.
                ' if you are unsure about how to use this form',
            ),
        'why_render_arrays' => array(
            '#items' => $list,
            '#title' => 'Use these codes in your form',
            '#theme' => 'item_list',
            ),
        );

    return drupal_render($build);
}


/**
* Form for submitting and editing details of each feed
*
*/

function affiliate_hoover_uniqueform_build_form($form, &$form_state) {

    $form_data = affiliate_hoover_return_form_details();

    // find available not types
    $node_types = node_type_get_types();

    $total_types = array();

    foreach ($node_types as $key => $value) {

        $total_types[$key] = $key;

    }

    // find all vocabularies
    $vocabulary = taxonomy_get_vocabularies();
    $checklist_vocab_array = array();
    /* Change to array('0' => '--none--'); if you want a none option*/
    foreach ($vocabulary as $item) {

        $key = $item->vid;
        $value = $item->name;
        $checklist_vocab_array[$key] = $value;

    }

    // list available filters
    $filters = filter_formats();
    $filter_array = array();

    foreach ($filters as $item) {
        $key = $item->format;
        $value = $item->name;
        $filter_array[$key] = $value;

    }

    $form['affiliate_hoover_uniqueform_build_form'] = array(
        '#type' => 'item',
        '#description' => 'Some description here for form intro',
        '#post_render' => array('affiliate_hoover_pages_render_array'),
        );

    $form['first_part'] = array(
        '#type' => 'fieldset',
        '#title' => t('Feed details for '.arg(4)),
        '#description' => 'Fieldset description here',
        );

    $form['first_part']['form_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#required' => TRUE,
        '#maxlength' => 255,
        '#description' => t('Do not add anything other than the above codes for a title'),
        '#default_value' => !empty($_REQUEST['form_title']) ? check_plain($_REQUEST['form_title']) :
            check_plain($form_data->form_title),
        );

    $form['first_part']['form_title_contains'] = array(
        '#type' => 'textfield',
        '#title' => t('Form title contains'),
        '#maxlength' => 255,
        '#description' => t('Title contains keywords (comma seperated list):'),
        '#default_value' => !empty($_REQUEST['form_title_contains']) ? check_plain($_REQUEST['form_title_contains']) :
            check_plain($form_data->form_title_contains),
        );

    $form['first_part']['form_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Post body'),
        '#required' => TRUE,
        '#description' => t('You can use HTML in here. Examples:<br>To place an image: '.
            htmlspecialchars("<img src=\"[#7#]\">")."<br>To create a link: ".htmlspecialchars("<a href=\"[#5#]\">[#1#]</a>").
            '<br>If you are going to include internal links you must write the full URL, ie '.
            htmlspecialchars("http://www.example.com/category/page-here")),
        '#default_value' => !empty($_REQUEST['form_body']) ? $_REQUEST['form_body'] : $form_data->
            form_body,
        );


    $form['first_part']['form_nofollow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Nofollow'),
        '#description' => t('Turn links in the body text into nofollow'),
        '#default_value' => !empty($_REQUEST['form_nofollow']) ? $_REQUEST['form_nofollow'] : $form_data->
            form_body_nofollow,
        );

    $form['first_part']['form_body_contains'] = array(
        '#type' => 'textfield',
        '#title' => t('Post body contains'),
        '#maxlength' => 255,
        '#description' => t('Body contains keywords (comma seperated list):'),
        '#default_value' => !empty($_REQUEST['form_body_contains']) ? check_plain($_REQUEST['form_body_contains']) :
            check_plain($form_data->form_body_contains),
        );

    $form['first_part']['form_body_filter'] = array(
        '#type' => 'select',
        '#title' => t('Filter'),
        '#description' => t('Pick which filter to use when parsing the individual items'),
        '#default_value' => !empty($_REQUEST['form_body_filter']) ? $_REQUEST['form_body_filter'] :
            $form_data->form_body_filter,
        '#required' => TRUE,
        '#options' => $filter_array,
        );

    $form['first_part']['form_categories_parent'] = array(
        '#type' => 'select',
        '#title' => t('Vocabulary'),
        '#required' => TRUE,
        '#description' => t('Pick a vocabulary'),
        '#default_value' => !empty($_REQUEST['form_categories_parent']) ? $_REQUEST['form_categories_parent'] :
            $form_data->form_categories_parent,
        '#options' => $checklist_vocab_array,
        );

    $form['first_part']['form_taxonomy'] = array(
        '#type' => 'textfield',
        '#title' => t('Taxonomy'),
        '#maxlength' => 255,
        '#description' => t('Can be either text or code. All values must be separated with a comma'),
        '#default_value' => !empty($_REQUEST['form_taxonomy']) ? check_plain($_REQUEST['form_taxonomy']) :
            check_plain($form_data->form_categories),
        );

    $form['first_part']['post_status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Status'),
        '#description' => t('Click to immediately publish the item. Otherwise, create item and manually switch to publish one at a time'),
        '#default_value' => !empty($_REQUEST['post_status']) ? $_REQUEST['post_status'] : $form_data->
            post_status,
        );

    $form['first_part']['form_promote'] = array(
        '#type' => 'checkbox',
        '#title' => t('Promote'),
        '#description' => t('Indicating whether the node should be displayed on the front page.'),
        '#default_value' => !empty($_REQUEST['form_promote']) ? $_REQUEST['form_promote'] : $form_data->
            form_promote,
        );

    $form['first_part']['form_comment'] = array(
        '#type' => 'checkbox',
        '#title' => t('Comment'),
        '#description' => t('Whether comments are allowed on this node'),
        '#default_value' => !empty($_REQUEST['form_comment']) ? $_REQUEST['form_comment'] : $form_data->
            form_allow_comments,
        );

    $form['first_part']['content_type'] = array(
        '#type' => 'select',
        '#title' => t('Content type'),
        '#description' => t('What content type should this feed be allocated to?'),
        '#default_value' => !empty($_REQUEST['content_type']) ? $_REQUEST['content_type'] : $form_data->
            post_type,
        '#options' => $total_types,
        );

    $form['first_part']['form_minrows'] = array(
        '#type' => 'textfield',
        '#title' => t('Start on this row'),
        '#maxlength' => 11,
        '#description' => t('Start processing on which row? (Out of a total of '.$form_data->
            num_rows.' entries. If these two fields are left blank then it will parse it all)'),
        '#default_value' => !empty($_REQUEST['form_minrows']) ? check_plain($_REQUEST['form_minrows']) :
            check_plain($form_data->min_rows),
        );

    $form['first_part']['form_maxrows'] = array(
        '#type' => 'textfield',
        '#title' => t('End on this row'),
        '#maxlength' => 11,
        '#description' => t('End processing on which row? (Out of a total of '.$form_data->num_rows.
            ' entries)'),
        '#default_value' => !empty($_REQUEST['form_maxrows']) ? check_plain($_REQUEST['form_maxrows']) :
            check_plain($form_data->max_rows),
        );

    $form['update'] = array(
        '#type' => 'submit',
        '#value' => 'Update and create',
        '#submit' => array('affiliate_hoover_uniqueform_build_form_submit'));

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Create only',
        '#submit' => array('affiliate_hoover_uniqueform_build_form_two_submit'),
        );

    $form['synchronize'] = array(
        '#type' => 'submit',
        '#value' => 'synchronize',
        '#submit' => array('affiliate_hoover_uniqueform_build_form_three_submit'),
        );

    return $form;

}


/**
 * Validation for unmanaged file form.
 */
function affiliate_hoover_uniqueform_build_form_validate($form, &$form_state) {

    if (!preg_match("/^\[#([0-9]*)#\]$/", trim($form_state['values']['form_title']))) {

        form_set_error('form_title', t('Only include one code such as [#3#] and nothing else for the title. You can change the title once the form has been created.'));

    }

    if ((empty($form_state['values']['form_minrows']) && !empty($form_state['values']['form_maxrows'])) ||
        (!empty($form_state['values']['form_minrows']) && empty($form_state['values']['form_maxrows']))) {

        form_set_error('form_minrows', t('Please make sure that set both a min rows number and a max rows number'));

    }

    if ($form_state['values']['form_minrows'] != '' && $form_state['values']['form_maxrows'] != '') {

        if (!ctype_digit($form_state['values']['form_minrows']) || !ctype_digit($form_state['values']['form_maxrows'])) {

            form_set_error('form_minrows', t('Make sure that you only use digits for the min and max rows'));

        }

    }

}


/**
 * Implementation of hook_submit())
 * 
 * Submit for updating AND creating nodes
 * 
 */
function affiliate_hoover_uniqueform_build_form_submit($form, &$form_state) {
    
    $startTime = microtime(TRUE);
                 
    affiliate_hoover_update_ah_feed_details($form_state);

    affiliate_hoover_process_form();
    
    $endTime = microtime(TRUE);
    
    $elapsed = $endTime - $startTime;

    drupal_set_message(t('Update in :seconds seconds', array(':seconds' => $elapsed)));

}

/**
 * Implementation of hook_submit())
 * 
 * Submit for ONLY creating new nodes
 * 
 */
function affiliate_hoover_uniqueform_build_form_two_submit($form, &$form_state) {
    
    $startTime = microtime(TRUE);

    affiliate_hoover_update_ah_feed_details($form_state);

    affiliate_hoover_process_form(FALSE);
    
    $endTime = microtime(TRUE);
    
    $elapsed = $endTime - $startTime;

    drupal_set_message(t('Updated in :seconds seconds', array(':seconds' => $elapsed)));

}

/**
 * Implementation of hook_submit())
 * 
 * Submit function synchronising the new file with currently published nodes
 * 
 */


function affiliate_hoover_uniqueform_build_form_three_submit($form, &$form_state) {

    $id_array_ah_total_feeds = db_query('SELECT n.nid FROM {ah_total_feeds} n WHERE n.cat_id = :cat_id',
        array(':cat_id' => hexdec(substr(md5(arg(4)), 0, 7))))->fetchAll();

    $id_array_nodes = db_query('SELECT n.nid FROM {node} n')->fetchAll();

    // This is to make sure that all nodes are live and have not been deleted

    $total_nid = array_intersect_assoc(affiliate_hoover_obj2array($id_array_ah_total_feeds),
        affiliate_hoover_obj2array($id_array_nodes));

    $file_entry = db_query('SELECT n.fileName, n.header_array, n.form_title FROM {ah_feed_details} n WHERE n.name = :name',
        array(':name' => arg(4)))->fetchAssoc();

    if ($file_entry['form_title'] == FALSE || $file_entry['form_title'] == NULL) {

        die('You have not created a prefered title for your feed');

    }

    $digit = preg_replace('/[^0-9]|,[0-9]*$/', '', $file_entry['form_title']);

    foreach ($total_nid as $result) {

        $total_posts[] = db_query('SELECT n.post_title_id FROM {ah_total_feeds} n WHERE n.nid = :id',
            array(':id' => (int)implode('', array_values($result))))->fetchAssoc();
    }

    //reduce the multidimensial array prodcued by the above database call

    foreach ($total_posts as $key => $value) {

        foreach ($value as $n_key => $n_value) {

            $reduced[] = (int)$n_value;

        }

    }

    $csv = new File_CSV_DataSource;

    $total_titles = array();

    if ($csv->load(AH_DIRECTORY.'/'.$file_entry['fileName'])) {

        $csv->symmetrize();

        $total = new ArrayIterator($csv->getrawArray());

        $total_meta = array();


        foreach ($total as $result => $value) {

            if ($result == 0) continue;

            foreach ($value as $key => $row_value) {

                if (stristr((string )$digit, (string )$key)) {
                    $total_titles[] = hexdec(substr(md5($row_value), 0, 7));

                }

            } // end foreach

        } // end foreach


        $difference = array_diff($reduced, $total_titles);

        if (!empty($difference)) {

            foreach ($difference as $result) {

                var_dump($result);

                static $x = 1;

                $posts_to_delete = db_query('SELECT n.nid FROM {ah_total_feeds} n WHERE n.post_title_id  = :post_title_id ',
                    array('post_title_id' => $result))->fetchObject();

                node_delete($posts_to_delete->nid);
                $total_delete = $x++;

            } // end foreach

            if (isset($total_delete)) {

                affiliate_hoover_write_file($total_delete." posts from ".arg(4).
                    " were deleted after synchronization on ".date('h:i:s A, l jS \of F Y'));

            } // end if

        } // end if (!empty($difference)) {

    } //  if ($csv->load($directory.'/'.$file_entry['fileName'])) {

    drupal_set_message(t('A total of @number posts were deleted from the database after synchronization with the @name feed file',
        array('@number' => $total_delete, '@name' => arg(4))));

}


/**
* Turns objects into arrays
*
*/

function affiliate_hoover_obj2array($obj) {
    if (!function_exists('json_encode')) die('JSON Library is required to use this function!');
    return json_decode(json_encode($obj), true); // use true for assoc array.
}


/**
* Database insert for ah_feed_details
*
*/

function affiliate_hoover_update_ah_feed_details($form_state) {

    $update = db_update('ah_feed_details')->fields(array(
        'form_title' => $form_state['values']['form_title'],
        'form_title_contains' => $form_state['values']['form_title_contains'],
        'form_body' => $form_state['values']['form_body'],
        'form_body_filter' => $form_state['values']['form_body_filter'],
        'form_body_contains' => $form_state['values']['form_body_contains'],
        'form_body_nofollow' => $form_state['values']['form_nofollow'],
        'form_categories_parent' => $form_state['values']['form_categories_parent'],
        'form_categories' => $form_state['values']['form_taxonomy'],
        'form_allow_comments' => $form_state['values']['form_comment'],
        'form_promote' => $form_state['values']['form_promote'],
        'post_type' => $form_state['values']['content_type'],
        'min_rows' => (int)$form_state['values']['form_minrows'],
        'max_rows' => (int)$form_state['values']['form_maxrows'],
        'post_status' => $form_state['values']['post_status'],
        ))->condition('name', arg(4))->execute();

    return $update;

}


/**
* Creates unique nodes by using the data in the ah_feed_details table and the content of the connected CSV file
*
*/

function affiliate_hoover_process_form($bol = TRUE) {

    $item = db_query('SELECT n.* FROM {ah_feed_details} n WHERE n.name = :name', array(':name' =>
            arg(4)))->fetchObject();

    $headers = unserialize($item->header_array);

    global $user;
    global $language;

    $post_meta = affiliate_hoover_get_post_meta();

    $csv = new File_CSV_DataSource;

    if ($csv->load($item->url)) {

        $csv->symmetrize();

        $total = new ArrayIterator($csv->getrawArray());

        foreach ($total as $result => $value) {

            if (($item->min_rows != NULL && $item->max_rows != NULL) || $item->min_rows != '' && $item->max_rows != '') {

                if ($result > $item->max_rows) continue;
                if ($result < $item->min_rows) continue;

            }

            if ($result == 0) continue;

            $total_val = count($value);

            foreach ($value as $key => $row_value) {

                if ($key === 0) {
                    
                    //build node object here

                    $newNode = new stdClass();
                    $newNode->type = $item->post_type;
                    $newNode->uid = $user->uid;
                    $newNode->status = $item->post_status;
                    $newNode->sticky = 0;
                    $newNode->language = LANGUAGE_NONE;

                    node_object_prepare($newNode);

                    $post_title = $item->form_title;
                    $post_content = $item->form_body;
                    $post_category = $item->form_categories;
                    $post_type = $item->post_type;

                } // end if ($key === 0) {

                // Post title

                if (stristr($item->form_title, "[#$key#]") !== FALSE) {

                    $post_title = str_replace("[#$key#]", $row_value, $post_title);

                }

                // Post content

                if (stristr($item->form_body, "[#$key#]") !== FALSE) {

                    $post_content = str_replace("[#$key#]", $row_value, $post_content);

                }

                // form cats

                if (stristr($item->form_categories, "[#$key#]") !== FALSE) {

                    $post_category = str_replace("[#$key#]", $row_value, $post_category);

                }

                // Allow comments

                if ($item->form_allow_comments == TRUE) {
                    $comment_status = 2;
                } else {
                    $comment_status = 1;
                }

                if ($item->form_body_nofollow == TRUE) {
                    $nofollow = TRUE;
                } else {
                    $nofollow = FALSE;
                }

                static $i = 1;
                static $x = 1;

                if ($key === ($total_val - 1)) {

                    $title = stripslashes(affiliate_hoover_check_utf8($post_title));

                    $newNode->title = $title;

                    if ($nofollow) {

                        $body = stripslashes(affiliate_hoover_dont_follow_links(affiliate_hoover_add_tracking_link
                            (affiliate_hoover_check_utf8($post_content))));

                    } else {

                        $body = stripslashes(affiliate_hoover_add_tracking_link(affiliate_hoover_check_utf8
                            ($post_content)));

                    }

                    $newNode->body[$newNode->language]['0']['value'] = $body;
                    $newNode->body[$newNode->language]['0']['format'] = $item->form_body_filter;

                    $newNode->comment = $comment_status;

                    $duplicate = FALSE;

                    foreach ($post_meta as $post_meta_result) {

                        if ((int)$post_meta_result->post_title_id === hexdec(substr(md5($title), 0,
                            7))) {

                            if ($bol === FALSE) continue;

                            // this is set to FALSE if the update button is clicked
                            // that way no old posts will not be updated

                            // If the two values are the same then the post already exists

                            $update = TRUE;

                            // If the title and body DO contain keywords then publish items OR publish if NO keywords set

                            if ($update === TRUE) {

                                $update_node = node_load($post_meta_result->nid);
                                $update_node->type = $item->post_type;
                                $update_node->uid = $user->uid;
                                $update_node->sticky = 0;
                                $update_node->language = LANGUAGE_NONE;
                                $update_node->body[$newNode->language]['0']['value'] = $body;
                                $update_node->body[$newNode->language]['0']['format'] = $item->
                                    form_body_filter;
                                $update_node->comment = $comment_status;
                                $update_node->status = $item->post_status;
                                $update_node->changed = strtotime("now");

                                node_object_prepare($update_node);

                                //$newNode->nid = $post_meta_result->nid;

                                if ($item->form_categories != "") {

                                    $cat_array = explode(",", $post_category);

                                    $post_cats_id = array();

                                    foreach ($cat_array as $result) {

                                        $result = stripslashes(affiliate_hoover_check_utf8
                                            (trim($result)));

                                        // make sure that the user doesn't accidently add numbers
                                        if (!preg_match("/^[0-9]/", $result)) {

                                            // check to see if taxonomy term exists first
                                            $query = new EntityFieldQuery;
                                            $term_exists = $query->entityCondition('entity_type',
                                                'taxonomy_term')->propertyCondition('name', $result)->
                                                propertyCondition('vid', $item->
                                                form_categories_parent)->execute();

                                            if (!empty($term_exists)) {

                                                foreach ($term_exists as $new_result) {

                                                    $newNode->field_tags[$newNode->language][]['tid'] = (int)
                                                        implode('', array_keys($new_result));

                                                }

                                            } else {

                                                $term = new stdClass();
                                                $term->name = drupal_substr($result, 0, 255);
                                                $term->vid = $item->form_categories_parent; // �1� is a vocabulary id you wish this term to assign to
                                                taxonomy_term_save($term); // Finally, save our term
                                                $newNode->field_tags[$newNode->language][]['tid'] = (int)
                                                    $term->tid;

                                            }

                                        } // end if  if (!preg_match("/^[0-9]/", $result)) {

                                    } // end foreach ($cat_array as $result) {

                                } // end if ($item->form_categories != "") {

                            } // end update

                            $total_cats = $i++;

                            $update_node->uid = $user->uid;

                            $node = node_save($update_node);

                            // reset cha
                            entity_get_controller('node')->resetCache(array($update_node->nid));

                            node_submit($node);
                            $update_node = NULL;
                            $newNode = NULL;
                            $node = NULL;

                            // If already exists then update item rather than create a new one.

                            $duplicate = TRUE;
                            break;

                        } // end if ((int)$result->meta_value === hexdec(substr(md5($post_title), 0, 7))) {

                    } // end foreach ($post_meta as $result) {

                    if ($duplicate === FALSE) {

                        // Here create variables for the keyword filtering on creating new post
                        $publish = TRUE;

                        if ($item->form_title_contains != '') {

                            $form_title_contains = explode(',', $item->form_title_contains);
                            $publish = FALSE;

                            foreach ($form_title_contains as $new_result) {

                                if (stristr($newNode->title, trim($new_result))) {
                                    $publish = TRUE;
                                }

                            }

                        } // end if ($item->form_title_contains != "") {

                        if ($item->form_body_contains != '') {

                            $form_body_contains = explode(',', $item->form_body_contains);
                            $update = FALSE;

                            foreach ($form_body_contains as $new_result) {

                                if (stristr($newNode->body[$newNode->language][0]['value'], trim($new_result))) {
                                    $publish = TRUE;
                                }

                            }

                        } // end if ($item->form_body_contains != "") {

                        // If the title and body DO contain keywords then publish items OR publish if NO keywords set

                        if ($publish === TRUE) {

                            // Categories

                            // Create the categories in the database here and use the IDs in the insert_post() function

                            if ($item->form_categories != '') {

                                $cat_array = explode(',', $post_category);

                                $post_cats_id = array();

                                foreach ($cat_array as $result) {

                                    $result = stripslashes(affiliate_hoover_check_utf8(trim($result)));

                                    // make sure that the user doesn't accidently add numbers
                                    if (!preg_match("/^[0-9]/", $result)) {

                                        // check to see if taxonomy term exists first
                                        $query = new EntityFieldQuery;
                                        $term_exists = $query->entityCondition('entity_type',
                                            'taxonomy_term')->propertyCondition('name', $result)->
                                            propertyCondition('vid', $item->form_categories_parent)->
                                            execute();

                                        if (!empty($term_exists)) {

                                            foreach ($term_exists as $new_result) {

                                                $newNode->field_tags[$newNode->language][]['tid'] = (int)
                                                    implode('', array_keys($new_result));

                                            }

                                        } else {

                                            $term = new stdClass();
                                            $term->name = drupal_substr($result, 0, 255);
                                            $term->vid = $item->form_categories_parent; // �1� is a vocabulary id you wish this term to assign to
                                            taxonomy_term_save($term); // Finally, save our term
                                            $newNode->field_tags[$newNode->language][]['tid'] = (int)
                                                $term->tid;

                                        }

                                    } // end if  if (!preg_match("/^[0-9]/", $result)) {

                                } // end foreach ($cat_array as $result) {

                            } // end if if ($item->form_categories != "") {

                            $newNode->created = strtotime("now");
                            $newNode->uid = $user->uid;

                            $return = db_query('SELECT n.id FROM {ah_total_feeds} n WHERE n.post_title_id = :post_title_id',
                                array(':post_title_id' => hexdec(substr(md5($post_title), 0, 7))))->
                                fetchObject();

                            if ($return == FALSE) {

                                $node = node_save($newNode);

                                db_insert('ah_total_feeds')->fields(array(
                                    'cat_id' => hexdec(substr(md5(arg(4)), 0, 7)),
                                    'post_title_id' => hexdec(substr(md5($post_title), 0, 7)),
                                    'nid' => $newNode->nid,
                                    ))->execute();

                                node_submit($node);
                                $total_new = $x++;

                            }

                            $newNode = NULL;
                            $node = NULL;

                        } // end if ($publish === TRUE) {

                    } // end if ($duplicate === FALSE) {

                } // end  if ($key === ($total_val - 1)) {

            } // end  foreach ($value as $key => $row_value) {

        } // end foreach ($total as $result => $value) {

        // write to file

        if (isset($total_new)) {
            affiliate_hoover_write_file($total_new." posts from ".$item->name." were created on ".
                date('h:i:s A, l jS \of F Y'));
        }

        if (isset($total_cats)) {
            affiliate_hoover_write_file($total_cats." posts from ".$item->name." were updated on ".
                date('h:i:s A, l jS \of F Y'));
        }


    } // end if ($csv->load() {

}


/**
 * Implements hook_file_validate().
 *
 * We're testing to see if the string 'test.' exists in the file name.
 */
/*
function affiliate_hoover_uniquename_build_form_file_validate($file, $string = 'test.') {
$errors = array();
if (strpos($file->filename, $string) !== FALSE) {
$errors[] = t('The string "%string" is reserved, please remove this string from your filename and try again.',
array('%string' => $string));
}
return $errors;
}
*/



/**
* adds nofollow to links in the HTML
*
*/

function affiliate_hoover_dont_follow_links($html) {
    // follow these websites only!
    $follow_list = array($_SERVER['HTTP_HOST']);
    return preg_replace('%(<a\s*(?!.*\brel=)[^>]*)(href="https?://)((?!(?:(?:www\.)?'.implode('|(?:www\.)?',
        $follow_list).'))[^"]+)"((?!.*\brel=)[^>]*)(?:[^>]*)>%', '$1$2$3"$4 rel="nofollow">', $html);
}

/**
* adds class of ah_link to outbound links,
* This is intended as for use with tracking clicks on outbound links
*
*/

function affiliate_hoover_add_tracking_link($html) {
    // follow these websites only!
    $follow_list = array($_SERVER['HTTP_HOST']);
    $out = preg_replace('/(<a[^>]*?)(class\s*\=\s*\"[^\"]*?\")([^>]*?>)/', '$1$3', $html);
    return preg_replace('%(<a\s*(?!.*\brel=)[^>]*)(href="https?://)((?!(?:(?:www.)?'.implode('|(?:www\.)?',
        $follow_list).'))[^"]+)"((?!.*\brel=)[^>]*)(?:[^>]*)>%', '$1$2$3"$4 class="ah_link ">', $out);

}



function affiliate_hoover_get_post_meta() {

    return db_query("
    SELECT * FROM {ah_total_feeds}")->fetchAll();

}

/**
* Checks to see if data is UTF8, if not it converts it
*/


function affiliate_hoover_check_utf8($str) {

    if (mb_detect_encoding($str, 'UTF-8', TRUE) == FALSE) {
        return utf8_encode($str);
    } else {
        return $str;
    }

}

/**
* Writes to file all node creation and deletation activity
*/

function affiliate_hoover_write_file($info) {

    $file = AH_DIRECTORY.'/log.txt';

    $fh = fopen($file, 'a+');
    fwrite($fh, $info."\n");
    fclose($fh);

}

/**
* Title callback from menu
*/

function affiliate_hoover_unique_form_title_callback($args) {

    return t($args);

}
