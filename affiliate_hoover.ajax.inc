<?php

/**
* @file
* The theme system, which controls the output of Drupal.
*
* The theme system allows for nearly all output of the Drupal system to be
* customized by user themes.
*/

function affiliate_hoover_ajax_callback() {

    $digit = preg_replace('/[^0-9]|,[0-9]*$/', '', $_REQUEST['post']);

    $query = db_insert('ah_tracking')->fields(array(
        'date' => time(),
        'post_id' => (int)$digit,
        'ip' => (string )$_SERVER['REMOTE_ADDR'],
        ))->execute();

    if ($query == '1') {

        return t('sucess');

    }

}
