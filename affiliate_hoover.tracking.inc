<?php

/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */


/**
 * Function for menu page callback
 *
 */

function affiliate_hoover_tracking_page() {

    return drupal_get_form('affiliate_hoover_tracker_form');

}


/**
 * Function called by drupal_get_form in affiliate_hoover_tracking_page
 *
 * @return
 *   Form array
 */


function affiliate_hoover_tracker_form($form, &$form_state) {

    if (variable_get('ah_tracking')) {

        $tracking = '1';

    } else {

        $tracking = '0';

    }

    $form['affiliate_hoover_tracker_form'] = array(
        '#type' => 'item',
        '#description' => 'Turn on and off tracking',
        '#post_render' => array('affiliate_hoover_tracker_table'),
        );

    $form['first_part'] = array(
        '#type' => 'fieldset',
        '#description' => 'Click the checkbox to turn tracking off',
        );

    $form['first_part']['tracking'] = array(
        '#type' => 'checkbox',
        '#title' => t('Tracking'),
        '#default_value' => !empty($_REQUEST['tracking']) ? $_REQUEST['tracking'] : $tracking,
        );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        );

    return $form;
}


/**
 * Implementation of hook_submit()
 * 
 */

function affiliate_hoover_tracker_form_submit($form, &$form_state) {

    if ($form_state['values']['tracking'] == 1) {

        variable_set('ah_tracking', TRUE);

        drupal_set_message(t('You have turned tracking off'));

    } elseif ($form_state['values']['tracking'] == 0) {

        variable_set('ah_tracking', FALSE);

        drupal_set_message(t('You have turned tracking on'));

    }

}


/**
 * Function called in post_render for the affiliate_hoover_tracker_form
 *
 * @return
 *   theme table and pager
 */

function affiliate_hoover_tracker_table() {

    $total = db_query('SELECT id FROM {ah_tracking}')->rowCount();

    $number_per_page = 10;

    $page = pager_default_initialize($total, $number_per_page);

    $offset = $number_per_page * $page;

    $results = db_query_range('SELECT n.ip, n.post_id, n.date FROM {ah_tracking} n', $offset, $number_per_page)->
        fetchAll();

    if ($results) {

        // reset array to first array key is 1 not 0
        $keys = range(1, count($results));
        $values = array_values($results);
        $results = array_combine($keys, $values);

        // Table header
        $header = array(
            t('Date'),
            t('Post'),
            t('ip'),
            );

        // Table rows

        $rows = array();

        static $i = 1;

        foreach ($results as $key => $value) {

            // work out time

            $time = format_date($value->date);

            // find node title

            $node = node_load($value->post_id);

            $new_array = array(
                $time,
                $node->title,
                $value->ip,
                );

            if (($i++ % 1) == 0) {

                $rows[] = $new_array;
                $new_array = null;

            }

        } // end foreach

        $build = array('string_paragraph' => array(
                '#type' => 'markup',
                '#markup' => t('<h4>Tracks clicks on affiliate links</h4><p>You can switch this off for users by using the form underneath the table</p>'),
                ), );

        $output = drupal_render($build);

        // Format and print out table.
        $output .= theme('table', array(
            'header' => $header,
            'rows' => $rows,
            ));

        $output .= theme('pager');

        return $output;

    } else {

        return t('You have no tracking results yet');

    } // end if($results) {


}
