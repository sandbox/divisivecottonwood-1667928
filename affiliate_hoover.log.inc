<?php

function affiliate_hoover_log_page() {

    return affiliate_hoover_read_top_lines();

}


function affiliate_hoover_read_top_lines() {

    $file = AH_DIRECTORY.'/log.txt';

    if (file_exists($file)) {

        $data = file($file);

    } else {
        
        $data = NULL;
        
    }

    if ($data != NULL) {

        $file = array_reverse($data);

        $header = array(t('Log entry'));

        $rows = array();

        static $i = 1;

        foreach ($file as $row) {

            $new_array = array(strip_tags($row));

            if (($i++ % 1) == 0) {

                $rows[] = $new_array;
                $new_array = null;

            }

            if ($i++ === 15) break;
        }
        
        $build = array('string_paragraph' => array(
                '#type' => 'markup',
                '#markup' =>
                    t('<h4>Logs entries for all deletion and creation of posts by Affiliate Hoover</h4>'),
                ), );

        $output = drupal_render($build);

        // Format and print out table.
        $output .= theme('table', array(
            'header' => $header,
            'rows' => $rows,
            ));
            
        return $output;

    } else {
        
        return t('No log yet');
        
    }// end if($file != NULL) {

}
